import {Player} from "../shared/model/player";
const Struct = require('struct');


interface RefreshXStruct {
    setBuffer: (buffer: Buffer) => void;
    length: () => number;
    fields: {
        players: {len: number, name: string}[],
        hwId: {len: number, name: string}[],
        team: number[],
        kills: number[],
        flags: number[],
        deaths: number[],
        ping: number[],
        id: number[],
        ip: {part: number[]}[],
        xPos: number[],
        yPos: number[],
        redFlagX: number,
        redFlagY: number,
        blueFlagX: number,
        blueFlagY: number,
        score: number[],
        map: {len: number, name: string},
        timeLimit: number,
        timeLeft: number,
        killLimit: number,
        gameMode: number,
        maxPlayers:number,
        maxSpectators:number,
        hasPassword:number,
        nextMap: {len: number, name: string}
    };
}

const refreshXStruct: RefreshXStruct = Struct()
    .array('players', 32, Struct()
        .word8Ule('len')
        .chars('name', 24))
    .array('hwId', 32, Struct()
        .word8Ule('len')
        .chars('name', 11))
    .array('team', 32, 'word8Ule')
    .array('kills', 32, 'word16Ule')
    .array('flags', 32, 'word8Ule')
    .array('deaths', 32, 'word16Ule')
    .array('ping', 32, 'word32Ule')
    .array('id', 32, 'word8Ule')
    .array('ip', 32, Struct()
        .array('part', 4, 'word8Ule'))
    .array('xPos', 32, 'floatle')
    .array('yPos', 32, 'floatle')
    .floatle('redFlagX')
    .floatle('redFlagY')
    .floatle('blueFlagX')
    .floatle('blueFlagY')
    .array('score', 4, 'word16Ule')
    .struct('map', Struct()
        .word8Ule('len')
        .chars('name', 16))
    .word32Ule('timeLimit')
    .word32Ule('timeLeft')
    .word16Ule('killLimit')
    .word8Ule('gameMode')
    .word8Ule('maxPlayers')
    .word8Ule('maxSpectators')
    .word8Ule('hasPassword')
    .struct('nextMap', Struct()
        .word8Ule('len')
        .chars('name', 16));

export class RefreshX {
    public static readonly LENGTH: number = refreshXStruct.length();

    players: Player[];

    score:number[];

    map: string;

    timeLimit: number;

    timeLeft: number;

    killLimit: number;

    gameMode: number;

    /**
     * Reads the data from the buffer and returns the new offset
     */
    read(data: Buffer, offset: number = 0): number {
        if (data.length - offset < RefreshX.LENGTH) {
            throw new Error(`Buffer must have ${RefreshX.LENGTH} bytes left (was ${data.length - offset})!`);
        }

        if (offset !== 0) {
            let buffer = new Buffer(RefreshX.LENGTH);
            data.copy(buffer, 0, offset);
            refreshXStruct.setBuffer(buffer);
        }
        else {
            refreshXStruct.setBuffer(data);
        }

        let fields = refreshXStruct.fields;

        this.players = [];
        for (let i = 0; i < 32; i++) {
            this.players[i] = {
                id: 0,
                name: fields.players[i].name
            };
        }
        this.players.forEach((player, i) => {
            player.id = fields.id[i];
        });
        this.players.forEach((player, i) => {
            player.team = fields.team[i];
        });
        this.players.forEach((player, i) => {
            player.kills = fields.kills[i];
        });
        this.players.forEach((player, i) => {
            player.flags = fields.flags[i];
        });
        this.players.forEach((player, i) => {
            player.deaths = fields.deaths[i];
        });
        this.players.forEach((player, i) => {
            player.ping = fields.ping[i];
        });
        this.players.forEach((player, i) => {
            player.xPos = fields.xPos[i];
        });
        this.players.forEach((player, i) => {
            player.yPos = fields.yPos[i];
        });
        this.players.forEach((player, i) => {
            let ip: number[] = fields.ip[i].part;
            player.ip = `${ip[0]}.${ip[1]}.${ip[2]}.${ip[3]}`;
        });
        this.score = fields.score;
        this.map = fields.map.name;
        this.timeLimit = fields.timeLimit;
        this.timeLeft = fields.timeLeft;
        this.killLimit = fields.killLimit;
        this.gameMode = fields.gameMode;
        return offset + RefreshX.LENGTH;
    }
}