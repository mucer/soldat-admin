import {Player} from "../shared/model/player";
const Struct = require('struct');


interface RefreshStruct {
    setBuffer: (buffer: Buffer) => void;
    length: () => number;
    fields: {
        players: {len: number, name: string}[],
        team: number[],
        kills: number[],
        deaths: number[],
        ping: number[],
        id: number[],
        ip: {part: number[]}[],
        score: number[],
        map: {len: number, name: string},
        timeLimit: number,
        currentTime: number,
        killLimit: number,
        gameMode: number
    };
}

const refreshStruct:RefreshStruct = Struct()
    .array('players', 32, Struct()
        .word8Ule('len')
        .chars('name', 24))
    .array('team', 32, 'word8Ule')
    .array('kills', 32, 'word16Ule')
    .array('deaths', 32, 'word16Ule')
    .array('ping', 32, 'word8Ule')
    .array('id', 32, 'word8Ule')
    .array('ip', 32, Struct()
        .array('part', 4, 'word8Ule'))
    .array('score', 4, 'word16Ule')
    .struct('map', Struct()
        .word8Ule('len')
        .chars('name', 16))
    .word32Ule('timeLimit')
    .word32Ule('currentTime')
    .word16Ule('killLimit')
    .word8Ule('gameMode');

export class Refresh {
    public static readonly LENGTH: number = refreshStruct.length();

    players: Player[];

    score:number[];

    map:string;

    timeLimit:number;

    timeLeft:number;

    killLimit:number;

    gameMode:number;

    /**
     * Reads the data from the buffer and returns the new offset
     */
    read(data: Buffer, offset: number = 0):number {
        if (data.length - offset < Refresh.LENGTH) {
            throw new Error(`Buffer must have ${Refresh.LENGTH} bytes left (was ${data.length - offset})!`);
        }

        if (offset !== 0) {
            let buffer = new Buffer(Refresh.LENGTH);
            data.copy(buffer, 0, offset);
            refreshStruct.setBuffer(buffer);
        }
        else {
            refreshStruct.setBuffer(data);
        }

        let fields: any = refreshStruct.fields;

        this.players = [];
        for (let i = 0; i < 32; i++) {
            this.players[i] = {
                id: 0,
                name: fields.players[i].name
            };
        }
        this.players.forEach((player, i) => {
            player.id = fields.id[i];
        });
        this.players.forEach((player, i) => {
            player.team = fields.team[i];
        });
        this.players.forEach((player, i) => {
            player.kills = fields.kills[i];
        });
        this.players.forEach((player, i) => {
            player.deaths = fields.deaths[i];
        });
        this.players.forEach((player, i) => {
            player.ping = fields.ping[i];
        });
        this.players.forEach((player, i) => {
            let ip: number[] = fields.ip[i].part;
            player.ip = `${ip[0]}.${ip[1]}.${ip[2]}.${ip[3]}`;
        });
        this.score = fields.score;
        this.map = fields.map;
        this.timeLimit = fields.timeLimit;
        this.timeLeft = fields.timeLeft;
        this.killLimit = fields.killLimit;
        this.gameMode = fields.gameMode;
        return offset + Refresh.LENGTH;
    }
}