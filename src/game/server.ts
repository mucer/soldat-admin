import {Socket} from "net";
import {Refresh} from "./refresh";
import {Player} from "../shared/model/player";
import {Game} from "../shared/model/game";
import {GameMode} from "../shared/model/game-mode";
import {RefreshX} from "./refresh-x";

interface Action {
    regex: RegExp;
    cb?: (result: RegExpExecArray) => void;
    update?: boolean;
}

export class Server {
    version: string;

    game: Game = {};

    private actions: Action[];

    private socket: Socket;

    private refreshPending = false;

    constructor(private address: string, private password: string, private port = 23073) {
        this.actions = [
            {
                regex: /Server Version: (\d+\.\d+\.\d+)/,
                cb: result => {
                    this.version = result[1];
                    this.connected();
                }
            },
            {regex: /^Max players (is|changed to) (\d+)/, update: true, cb: ary => this.game.maxPlayers = +ary[2]},
            {regex: /^Friendly Fire (is|changed to) (\d+)/, cb: ary => this.game.friendlyFire = ary[2] === '1'},
            {regex: /^Current bonus frequency (is|changed to) (\d+)/, cb: ary => this.game.bonusFrequency = +ary[2]},
            {regex: /^Voting percent (is|changed to) (\d+)/, cb: ary => this.game.votingPercent = +ary[2]},
            {regex: /^Respawn time (is|changed to) (\d+)/, cb: ary => this.game.respawnTime = +ary[2]},
            {regex: /^Maximum Respawn time (is|changed to) (\d+)/, cb: ary => this.game.maxRespawnTime = +ary[2]},
            {regex: /^.+ joining game/, update: true},
            {regex: /^.+ has joined \w+ team/, update: true},
            {regex: /^.+ has left \w+ team/, update: true},
            {regex: /^.+ killed .+ with .+/, update: true},
            {regex: /^.+ scores for \w+ Team/, update: true},
            {regex: /^.+ captured the \w+ Flag/},
            {regex: /^.+ returned the \w+ Flag/},
            {regex: /^Time Left: \d+ minutes/},
            {regex: /^Soldat Admin Connection Established./},
            {regex: /^Welcome, you are in command of the server now./},
            {regex: /^List of commands available in the Soldat game Manual./},
            {regex: /^\//}
        ];
    }

    connect() {
        if (!this.socket) {
            this.socket = new Socket();
            this.socket.on('data', data => this.handleData(data));
            this.socket.connect(this.port, this.address, () => {
                console.log('Sending password');
                this.send(this.password);
            });
        }
    }

    private connected() {
        this.send('/friendlyfire');
        this.send('/bonus');
        this.send('/vote%');
        this.send('/respawntime');
        this.send('/maxrespawntime');
        this.send('/maxplayers');
    }

    send(cmd: string) {
        if (!this.socket) {
            throw new Error('Not connected');
        }
        console.log(`Sending: ${cmd}`);
        this.socket.write(`${cmd}\n`);
    }

    private handleData(data: Buffer) {

        let pos: number;
        let lastPos = 0;
        let requestUpdate = false;
        let server = this;

        while ((pos = data.indexOf('\n', lastPos + 1)) !== -1) {
            let buf = new Buffer(pos - lastPos - 1);
            data.copy(buf, 0, lastPos);
            // console.log(`from ${lastPos} to ${pos} of ${data.length}: ${buf.toString()}`);
            lastPos = pos + 1;
            let str = buf.toString('ascii');

            try {
                // RAW commands
                if (str === 'REFRESH') {
                    let refresh = new Refresh();
                    lastPos = refresh.read(data, lastPos);

                    this.game.players = refresh.players.filter(p => p.id !== 0);
                    this.game.score = refresh.score;
                    this.game.gameMode = GameMode.valueOf(refresh.gameMode);
                }
                else if (str === 'REFRESHX') {
                    let refreshX = new RefreshX();
                    lastPos = refreshX.read(data, lastPos);
                    this.refreshPending = false;

                    this.game.players = refreshX.players.filter(p => p.id !== 0)
                    ;
                    this.game.score = refreshX.score;
                    this.game.gameMode = GameMode.valueOf(refreshX.gameMode);
                }
                else {
                    let found = false;
                    this.actions.forEach(action => {
                        let result = action.regex.exec(str);
                        if (result) {
                            found = true;
                            requestUpdate = requestUpdate || action.update;
                            if (action.cb) {
                                action.cb.call(server, result);
                                console.log(`Calling action for '${str}'`);
                            }
                        }
                    });

                    if (!found) {
                        throw new Error(`No actions for message '${str}'`);
                    }
                }
            } catch (e) {
                console.log('Error', e);
            }
        }

        if (requestUpdate && !this.refreshPending) {
            this.send('REFRESHX');
            this.refreshPending = true;
        }
    }
}