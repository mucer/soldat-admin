export class GameMode {
    static readonly DM = new GameMode(0, 'Deathmatch', 0);
    static readonly PM = new GameMode(1, 'Pointmatch', 0);
    static readonly TM = new GameMode(2, 'Teammatch', 4);
    static readonly CTF = new GameMode(3, 'Capture the flag', 2);
    static readonly RM = new GameMode(4, 'Rambomatch', 0);
    static readonly INF = new GameMode(5, 'Infiltration', 2);
    static readonly HTF = new GameMode(6, 'Holt the flag', 2);
    static readonly VALUES = [GameMode.DM, GameMode.PM, GameMode.TM, GameMode.CTF, GameMode.RM, GameMode.INF, GameMode.HTF];

    static valueOf(id: any): GameMode {
        return GameMode.VALUES.find(g => g.id === +id);
    }

    constructor(readonly id: number, readonly name: string, readonly teams:number) {
    }

    isTeamGame() {
        return this.teams > 0;
    }
}