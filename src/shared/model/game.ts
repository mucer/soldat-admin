import {Player} from "./player";
import {GameMode} from "./game-mode";

export class Game {
    players?: Player[];

    friendlyFire?: boolean;

    maxPlayers?: number;

    bonusFrequency?: number;

    votingPercent?: number;

    respawnTime?: number;

    maxRespawnTime?: number;

    map?: string;

    gameMode?: GameMode;

    score:number[];
}