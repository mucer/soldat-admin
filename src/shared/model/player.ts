export interface Player {
    id: number;
    name: string;
    hwId?: string;
    team?: number;
    kills?: number;
    flags?: number;
    deaths?: number;
    ping?: number;
    ip?: string;
    xPos?: number;
    yPos?: number;
}