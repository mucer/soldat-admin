import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import {Server} from "./game/server";

let app = express();
app.use(bodyParser.json());

let server = new Server('sapp-dev-mon-scrum2', 'pwd');
server.connect();

app.use(express.static(path.join(__dirname, 'client/static')));
app.get('/api/friendlyfire', (req, res) => {
    let mode = 'on' in req.query ? '1' : ('off' in req.query ? '0' : '');
    server.send('/friendlyfire ' + mode);
    res.sendStatus(200);
});
app.get('/api/game', (req, res) => {
    res.json(server.game);
});
app.post('/api/bot', (req, res) => {
    let name: string = req.params['name'];
    let team: number = +req.params['team'];

    if (!isNaN(team) && server.game.gameMode.isTeamGame()) {
        server.send(`/addbot${team} ${name}`);
    }
    else {
        server.send(`/addbot ${name}`);
    }
});

app.listen(3000, () => console.log('Listening to 3000'));